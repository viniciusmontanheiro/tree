'use strict';

var express = require('express'),
    app = express(),
    server = require('http').createServer(app);
require('./config/express')(app);
require('./app/routes')(express);

server.listen(app.get("port"), function (err) {
    if (err) {
        throw new Error(':-( Server DOWN!!' + err);
    }
    console.log('Server listening at port %d', app.get("port"));
});

// Expose app
module.exports = app;