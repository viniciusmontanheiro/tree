
'use strict';

var bodyParser = require('body-parser'),
 methodOverride = require('method-override'),
 path = require('path'),
 fs = require("fs");

module.exports = function(app) {
  app.set('views', path.normalize(__dirname + '/../public/views'));
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.set("port",8080);
};